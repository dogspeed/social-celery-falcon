# social celery falcon
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import bcrypt
import json
from sqlalchemy import Column, BigInteger, String
from sqlalchemy.dialects.postgresql import UUID
import uuid

from .base import Base


class User(Base):
    """Model of user on database

    Attributes
    ----------
    __tablename__ : str
    id : sqlalchemy.Column
    user_uuid : sqlalchemy.Column
    email : sqlalchemy.Column
    username : sqlalchemy.Column
    password_hash : sqlalchemy.Column

    Methods
    -------
    set_password(self, password) : Sets user password
    check_password(self, password) : Checks user password
    to_json(self) : Returns user to json format
    """

    __tablename__ = 'users'

    id = Column(BigInteger, primary_key=True)
    user_uuid = Column(UUID(as_uuid=True), default=uuid.uuid4, unique=True,
                       nullable=False, index=True)
    email = Column(String, unique=True, nullable=False, index=True)
    username = Column(String, unique=True, nullable=False, index=True)
    password_hash = Column(String, nullable=False)

    def set_password(self, password):
        """Sets user password

        Parameters
        ----------
        password : str
            Password
        """
        self.password_hash = bcrypt.hashpw(password.encode('utf-8'),
                                           bcrypt.gensalt()).decode()

    def check_password(self, password):
        """Checks user password

        Parameters
        ----------
        password : str
            Password
        """
        return bcrypt.checkpw(password.encode('utf-8'),
                              str(self.password_hash).encode('utf-8'))

    def to_json(self):
        """Returns user in JSON format
        """
        return json.dumps(
            {
                'user_uuid': str(self.user_uuid),
                'email': self.email,
                'username': self.username,
            }
        )
