# social celery falcon
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from dotenv import load_dotenv
import falcon
import falcon_cors
import falcon_sqla
import os
import sqlalchemy

from .resources import RegisterResource, LoginResource


def create_app(database_url):
    """Creates the Falcon app

    Parameters
    ----------
    database_url : str
        Database URL
    """
    cors = falcon_cors.CORS(allow_all_origins=True, allow_all_headers=True,
                            allow_all_methods=True)

    engine = sqlalchemy.create_engine(database_url)
    manager = falcon_sqla.Manager(engine)

    api = falcon.API(middleware=[cors.middleware, manager.middleware])

    login_resource = LoginResource()
    api.add_route('/login', login_resource)
    register_resource = RegisterResource()
    api.add_route('/register', register_resource)

    return api


def get_app():
    """Returns a falcon application
    """
    load_dotenv()

    if not os.getenv('DATABASE_URL'):
        print("missing environment variable DATABASE_URL")
        return None

    return create_app(os.getenv('DATABASE_URL'))
