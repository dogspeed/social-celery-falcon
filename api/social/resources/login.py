# social celery falcon
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import falcon
import json

from social.models import User


class LoginResource(object):
    """Resource to login users

    Methods
    -------
    on_post(self, req, resp) : POST method handler
    """

    def on_post(self, req, resp):
        """POST method handler

        Parameters
        ----------
        req - Falcon request
            Request
        resp - Falcon response
            Response
        """
        body = req.stream.read()
        try:
            payload = json.loads(body.decode('utf-8'))
        except Exception:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Body contains a bad JSON'})
            return
        username = payload.get('username')
        if not username:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Missing username on request'})
            return
        password = payload.get('password')
        if not password:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Missing password on request'})
            return
        try:
            user = req.context.session.query(User).filter(
                User.username == username).one()
        except Exception:
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({'msg': 'User not found'})
            return
        if not user.check_password(password):
            resp.status = falcon.HTTP_404
            resp.body = json.dumps({'msg': 'User not found'})
            return
        resp.status = falcon.HTTP_200
        resp.body = user.to_json()
