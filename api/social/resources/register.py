# social celery falcon
# Copyright (C) 2020 Bruno Mondelo Giaramita
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import falcon
import json
import logging

from social.models import User


class RegisterResource(object):
    """Resource to register users

    Attributes
    ----------
    __logger__ : gunicorn logger

    Methods
    -------
    __init__(self) : constructor
    on_post(self, req, resp) : POST method handler
    """

    def __init__(self):
        """Constructor
        """
        self.__logger__ = logging.getLogger('gunicorn.error')

    def on_post(self, req, resp):
        """POST method handler

        Parameters
        ----------
        req - Falcon request
            Request
        resp - Falcon response
            Response
        """
        body = req.stream.read()
        try:
            payload = json.loads(body.decode('utf-8'))
        except Exception:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Body contains a bad JSON'})
            return
        email = payload.get('email')
        if not email:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Missing email on request'})
            return
        username = payload.get('username')
        if not username:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Missing username on request'})
            return
        password = payload.get('password')
        if not password:
            resp.status = falcon.HTTP_400
            resp.body = json.dumps({'msg': 'Missing password on request'})
            return
        try:
            user = req.context.session.query(User).filter(
                User.email == email).one()
            resp.status = falcon.HTTP_409
            resp.body = json.dumps({'msg': 'Email already in use'})
            return
        except Exception:
            pass
        try:
            user = req.context.session.query(User).filter(
                User.username == username).one()
            resp.status = falcon.HTTP_409
            resp.body = json.dumps({'msg': 'Username already in use'})
            return
        except Exception:
            pass
        user = User()
        user.email = email
        user.username = username
        user.set_password(password)
        try:
            req.context.session.add(user)
            req.context.session.commit()
        except Exception:
            resp.status = falcon.HTTP_500
            resp.body = json.dumps({'msg': 'Error creating user'})
            return
        try:
            user = req.context.session.query(User).filter(
                User.username == username).one()
        except Exception as e:
            self.__logger__.error('error creating user %s' % str(e))
            resp.status = falcon.HTTP_500
            resp.body = json.dumps({'msg': 'Error creating user'})
            return
        self.__logger__.info('created new user %s' % user.username)
        resp.status = falcon.HTTP_201
        resp.body = user.to_json()
